# Contributing to FirebaseESP32

The following is a set of guidelines for contributing to FirebaseESP32 and its packages, which are hosted in the [FirebaseESP32 Organization](https://gitlab.com/blacxcode/FirebaseESP32) on GitLab. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Code of Conduct

This project and everyone participating in it is governed by the [FirebaseESP32 Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [crack.codec@gmail.com](mailto:crack.codec@gmail.com).